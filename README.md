# Simple AddressBook Proof of Concept by Matthew McGahan

This is a project to demonstrate basic Add and Retrieve Contact functionality for a simulated Addressbook platform. 

## Installation

Clone repo from bitbucket using this command and URL:

git clone https://bitbucket.org/mmcgahan1/simple-addressbook-rest-api.git

After successfully cloning, start the Spring Application by running maven in the main ssimple-addressbook-rest-api folder by using the command (RECCOMENDED):

```bash
mvn spring-boot:run
```
Alternatively run the java .jar EXECUTABLE file from the same main folder by using this command from CMD:

```shell
java -jar simple-addressbook-rest-api-0.0.1
```

## Usage

## Endpoints

There are 2 endpoints GET /contact and POST /contact.

By Default starting the application with the mvn spring-boot:run command means that you can shoot JSON requests to localhost:8080.

### POST /contact

Contacts must be created in the POST /contact endpoint. A valid contact must contain the following parameters: firtname and surname

These are set as JSON in the body of the POST request. The parameters that must be sent and their formats are as follows:

"firstname" & "surname" : 
Sent as a string containing a less than 30 characters long and composed exclusively of alphabetic characters.


Example Valid Input
```JSON
{
	
	"firstname": "Foo",

	"currency": "Bar",

	"contactDescription": "Friendy neighborhood spiderman",

	"phoneNumber": "07369258147",

	"email": "tester@testing.com"

}
```
The ID is set internally and is returned in the response.

Example Output
```JSON
{
	
	"id": 1,

	"firstname": "Foo",

	"currency": "Bar",

	"contactDescription": "Friendy neighborhood spiderman",

	"phoneNumber": "07369258147",

	"email": "tester@testing.com"

}
```

### GET /contact

The GET /contact Endpoint is simple to use. To retrieve a previously created contact put the surname query param in the url of the contact you wish to retrieve.
for example a valid url would look like this with the default setup: localhost:8080/contact?surname=Bar

## Testing

In the Postman API Tests folder there are a range of postman tests that cover both endpoints for a variety of different error cases and success cases. Feel Free to use these to easily test the code when it is running.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)