package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.utils;

import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorCodes;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorConstants;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.Contact;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.ContactError;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test Class for ContactUtils, has 100% code coverage for all sub methods and logic
 * Further improvements: add coverage for error building.
 */
public class ContactUtilsTest {

    @Test
    public void testInvalidNameInvalidCharacters() {
        String stringWithInvalidNumCharacters = "MyName1";
        String stringWithInvalidNonANCharacters = "My@Name";
        String stringWithInvalidSpaceCharacters = "My Name";
        String stringWithInvalidBlankCharacters = "";

        assertFalse(ContactUtils.isValidName(stringWithInvalidNumCharacters));
        assertFalse(ContactUtils.isValidName(stringWithInvalidNonANCharacters));
        assertFalse(ContactUtils.isValidName(stringWithInvalidSpaceCharacters));
        assertFalse(ContactUtils.isValidName(stringWithInvalidBlankCharacters));
    }

    @Test
    public void testInvalidNameInvalidLength() {
        String stringWithInvalidLength = "abcdefghijklmnopqrstuvwxyz"
                + "abcdefghijklmnopqrstuvwxyz".toUpperCase();
        assertFalse(ContactUtils.isValidName(stringWithInvalidLength));
    }

    @Test
    public void testValidNameLength() {
        String nameWithValidCharacters = "Matthew";
        assertTrue(ContactUtils.isValidName(nameWithValidCharacters));
    }

    @Test
    public void testValidEmails() {
        assertTrue(ContactUtils.isValidEmail("user@domain.com"));
        assertTrue(ContactUtils.isValidEmail("user@domain.co.uk"));
        assertTrue(ContactUtils.isValidEmail("user.name@domain.com"));
        assertTrue(ContactUtils.isValidEmail("user.name@domain.co.uk"));
    }

    @Test
    public void testInvalidEmails() {
        assertFalse(ContactUtils.isValidEmail(".username@domain.com"));
        assertFalse(ContactUtils.isValidEmail("username@domain.com."));
        assertFalse(ContactUtils.isValidEmail("username@domain..com"));
    }

    @Test
    public void testValidDescription() {
        assertTrue(ContactUtils.isValidDescription("This is a valid description"));
        assertTrue(ContactUtils.isValidDescription("This is also a valid description containing numbers 12345"));
    }

    @Test
    public void testInvalidDescription() {
        assertFalse(ContactUtils.isValidDescription("This is not a valid description!"));
        assertFalse(ContactUtils.isValidDescription("This is also an invalid description containing numbers 12345."));
        assertFalse(ContactUtils.isValidDescription("This is also an invalid description containing altérnativé chars like ú"));
        assertFalse(ContactUtils.isValidDescription("Invalid Contact Desctiption due to length being over max length"
        + "IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII"
        + "IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII"));
    }
    @Test
    public void testValidPhoneNumbers() {
        assertTrue(ContactUtils.isValidPhoneNumber("447777777777"));
        assertTrue(ContactUtils.isValidPhoneNumber("07777777777"));
    }

    @Test
    public void testInvalidPhoneNumbers() {
        assertFalse(ContactUtils.isValidPhoneNumber("A7777777777"));
        assertFalse(ContactUtils.isValidPhoneNumber("00007777777777"));
        assertFalse(ContactUtils.isValidPhoneNumber("077777"));
    }

    @Test
    public void testMissingMandatoryParameterCheckReject() {
        Contact contact = createInvalidContact();
        ContactError contactError = ContactUtils.containsAllMandatoryParameters(contact);
        assertNotNull(contactError);
        assertEquals(ContactErrorCodes.MISSING_MANDATORY_PARAMETERS, contactError.getCode());
        assertEquals(ContactErrorConstants.MISSING_MANDATORY_PARAMETERS, contactError.getTitle());
        assertEquals("The mandatory parameter(s) [firstname, surname] are not provided", contactError.getErrorDescription());
    }
    @Test
    public void testMissingMandatoryParameterCheckSuccess() {
        Contact contact = createInvalidContact();
        contact.setFirstname("firstName");
        contact.setSurname("surname");
        ContactError contactError = ContactUtils.containsAllMandatoryParameters(contact);
        assertNull(contactError);
    }

    private Contact createInvalidContact() {
        Contact contact = new Contact();
        contact.setId(1);
        contact.setEmail("test@testing.com");
        contact.setPhoneNumber("07777777777");
        return contact;
    }

    @Test
    public void testInputWithIdRejected() {
        Contact contact = createInvalidContact();
        ContactError contactError = ContactUtils.checkRequestIsValid(contact);
        assertNotNull(contactError);
        assertEquals(ContactErrorCodes.CONTACT_ID_NOT_ALLOWED, contactError.getCode());
        assertEquals(ContactErrorConstants.CONTACT_ID_NOT_ALLOWED, contactError.getTitle());
        assertEquals(ContactErrorConstants.CONTACT_ID_NOT_ALLOWED, contactError.getErrorDescription());
    }
}