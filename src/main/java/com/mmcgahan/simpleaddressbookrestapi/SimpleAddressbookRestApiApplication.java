package com.mmcgahan.simpleaddressbookrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleAddressbookRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleAddressbookRestApiApplication.class, args);
	}

}
