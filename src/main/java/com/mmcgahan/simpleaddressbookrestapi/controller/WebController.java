package com.mmcgahan.simpleaddressbookrestapi.controller;


import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorCodes;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorConstants;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.Contact;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.ContactError;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.utils.ContactUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;


@RestController
public class WebController {

    private Map<Integer, Contact> listOfContacts;

    private AtomicInteger currentId;

    /**
     *
     * @param inputContact
     *  Requested Contact to add to addressbook
     * @return
     *  Return success response if contact added, otherwise return appropriate error.
     */
    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public ResponseEntity createContact(@RequestBody Contact inputContact) {
        initialiseIdAndStorage();
        ContactError contactError = ContactUtils.checkRequestIsValid(inputContact);
        if (contactError == null) {
            contactError = ContactUtils.checkForDuplicateContact(inputContact, listOfContacts);
        }
        if (contactError != null) {
            return ResponseEntity.badRequest().body(contactError);
        }
        putContactInList(inputContact);
        return ResponseEntity.status(HttpStatus.CREATED).body(inputContact);
    }

    /**
     *
     * @param surname
     *  Surname of contact to be searched in addressbook
     * @return
     *  Returns the contact details if found, else error or 404.
     */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public ResponseEntity retrieveContact(@RequestParam(value = "surname") String surname) {
        if (listOfContacts.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (ContactUtils.isValidName(surname)) {
            //Retrieves contact from memory if surname is present.
            Optional<Contact> foundContact = listOfContacts.values().stream()
                    .filter(contact -> contact.getSurname().toUpperCase().equals(surname.toUpperCase()))
                    .findFirst();
            //Returns 200 OK with Contact in body if found, else 404 not found
            if (foundContact.isPresent()) {
                return ResponseEntity.ok(foundContact.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            //Surname Invalid, so returning detailed error response.
            ContactError invalidNameError = new ContactError(
                    ContactErrorCodes.INVALID_NAME_FORMAT,
                    ContactErrorConstants.INVALID_NAME_FORMAT_TITLE,
                    String.format(ContactErrorConstants.INVALID_NAME_FORMAT_DESC, surname));
            return ResponseEntity.badRequest().body(invalidNameError);
        }
    }
    private void initialiseIdAndStorage() {
        if (currentId == null || currentId.intValue() == Integer.MAX_VALUE) {
            this.currentId = new AtomicInteger(1);
        }
        if (listOfContacts == null) {
            listOfContacts = new HashMap<>();
        }
    }
    private void putContactInList(Contact inputContact) {
        inputContact.setId(currentId.getAndIncrement());
        listOfContacts.put(inputContact.getId(), inputContact);
    }
}
