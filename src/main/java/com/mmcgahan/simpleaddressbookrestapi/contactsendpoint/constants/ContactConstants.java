package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants;

public class ContactConstants {
    /**
     * RFC 5322 Compliant email regex for storing emails safely
     */
    public static final String EMAIL_REGEX = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    /**
     * Phone Regex matching domestic and international phone numbers
     */
    public static final String PHONE_REGEX = "(0|([1-9]?[0-9]))?[0-9]{10}";

    /**
     * Name Regex matching any alphabetic character
     */
    public static final String NAME_REGEX = "[a-zA-Z]+";

    /**
     * Description regex to only contain alphanumeric characters
     */
    public static final String DESC_REGEX = "^[a-zA-Z0-9 ]+$";

    public static final int MAX_NAME_LENGTH = 30;

    public static final int MAX_DESC_LENGTH = 200;


}
