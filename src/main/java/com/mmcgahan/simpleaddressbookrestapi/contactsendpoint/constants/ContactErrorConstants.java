package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants;

public class ContactErrorConstants {

    public final static String MISSING_MANDATORY_PARAMETERS = "Missing mandatory parameters";

    public final static String DUPLICATE_CONTACT = "CONTACT IS DUPLICATE TO EXISTING CONTACT";

    public final static String INVALID_NAME_FORMAT_TITLE = "INVALID NAME";

    public final static String INVALID_NAME_FORMAT_DESC = "Name: %s, is invalid. Valid surnames are exclusively alphabetic and below "
            + ContactConstants.MAX_NAME_LENGTH
            + " characters in length.";

    public final static String CONTACT_ID_NOT_ALLOWED = "CONTACT ID IS NOT ALLOWED";

    public final static String INVALID_EMAIL_FORMAT = "INVALID EMAIL FORMAT";

    public final static String INVALID_EMAIL_FORMAT_DESC = "Enter a valid and RFC 5322 compliant email";

    public final static String INVALID_PHONE_NUMBER_FORMAT = "INVALID PHONE NUMBER FORMAT";

    public final static String INVALID_DESC_FORMAT = "INVALID DESC FORMAT";

    public final static String INVALID_DESC_FORMAT_DESC = "Contact Descriptions may only contain alphanumeric characters";
}
