package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants;

/**
 * Error code constant class to regulate error codes for the contacts endpoint.
 * To avoid confusion with generic HTTP response codes all error codes are 5 characters in length.
 *
 */
public class ContactErrorCodes {

    public final static int MISSING_MANDATORY_PARAMETERS = 50000;

    public final static int INVALID_NAME_FORMAT = 50001;

    public final static int CONTACT_ID_NOT_ALLOWED = 50002;

    public final static int INVALID_DESC_FORMAT = 50003;

    public final static int INVALID_PHONE_NUMBER_FORMAT = 50004;

    public final static int INVALID_EMAIL_FORMAT = 50005;

    public final static int DUPLICATE_ENTRY = 50006;

}
