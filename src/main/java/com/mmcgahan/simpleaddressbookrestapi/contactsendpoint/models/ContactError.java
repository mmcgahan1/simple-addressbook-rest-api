package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models;

public class ContactError {

    private final int code;

    private final String title;

    private final String errorDescription;

    public ContactError(int code, String title, String errorDescription) {
        this.code = code;
        this.title = title;
        this.errorDescription = errorDescription;
    }

    public ContactError(int code, String title) {
        this.code = code;
        this.title = title;
        this.errorDescription = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}

