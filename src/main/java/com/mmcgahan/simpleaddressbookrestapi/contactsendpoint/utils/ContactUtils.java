package com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.utils;

import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactConstants;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorCodes;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.constants.ContactErrorConstants;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.Contact;
import com.mmcgahan.simpleaddressbookrestapi.contactsendpoint.models.ContactError;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactUtils {
    public static ContactError checkRequestIsValid(Contact contact) {
        //Given id NOT ALLOWED
        if (contact.getId() != null) {
            return new ContactError(
                    ContactErrorCodes.CONTACT_ID_NOT_ALLOWED,
                    ContactErrorConstants.CONTACT_ID_NOT_ALLOWED);
        }
        //Mandatory Parameter Check
        ContactError contactError = containsAllMandatoryParameters(contact);
        if (contactError != null) {
            return contactError;
        }
        //Firstname Check
        if (!isValidName(contact.getFirstname())) {
            return new ContactError(
                    ContactErrorCodes.INVALID_NAME_FORMAT,
                    ContactErrorConstants.INVALID_NAME_FORMAT_TITLE,
                    String.format(ContactErrorConstants.INVALID_NAME_FORMAT_DESC, contact.getFirstname()));
        }
        //Surname Check
        if (!isValidName(contact.getSurname())) {
            return new ContactError(
                    ContactErrorCodes.INVALID_NAME_FORMAT,
                    ContactErrorConstants.INVALID_NAME_FORMAT_TITLE,
                    String.format(ContactErrorConstants.INVALID_NAME_FORMAT_DESC, contact.getSurname()));
        }
        //contactDescription check
        if (contact.getContactDescription() != null && !isValidDescription(contact.getContactDescription())) {
            return new ContactError(
                    ContactErrorCodes.INVALID_DESC_FORMAT,
                    ContactErrorConstants.INVALID_DESC_FORMAT,
                    ContactErrorConstants.INVALID_DESC_FORMAT_DESC);
        }
        //PhoneNumber check
        if (contact.getPhoneNumber() != null && !isValidPhoneNumber(contact.getPhoneNumber())) {
            return new ContactError(
                    ContactErrorCodes.INVALID_PHONE_NUMBER_FORMAT,
                    ContactErrorConstants.INVALID_PHONE_NUMBER_FORMAT);
        }
        //Email check
        if (contact.getEmail() != null && !isValidEmail(contact.getEmail())) {
            return new ContactError(
                    ContactErrorCodes.INVALID_EMAIL_FORMAT,
                    ContactErrorConstants.INVALID_EMAIL_FORMAT,
                    ContactErrorConstants.INVALID_EMAIL_FORMAT_DESC);
        }
        return null;
    }

    public static ContactError containsAllMandatoryParameters(Contact contact) {
        List<String> missingParams = new ArrayList<>();
        if (contact.getFirstname() == null) {
            missingParams.add("firstname");
        }
        if (contact.getSurname() == null) {
            missingParams.add("surname");
        }

        if (!CollectionUtils.isEmpty(missingParams)) {
            String errorMessage = "The mandatory parameter(s) " + missingParams.toString() + " are not provided";
            return new ContactError(
                    ContactErrorCodes.MISSING_MANDATORY_PARAMETERS,
                    ContactErrorConstants.MISSING_MANDATORY_PARAMETERS,
                    errorMessage);
        }
        return null;
    }

    public static boolean isValidName(String name) {
        if (name.length() > ContactConstants.MAX_NAME_LENGTH) {
            return false;
        }
        Pattern pattern = Pattern.compile(ContactConstants.NAME_REGEX);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
    public static boolean isValidDescription(String description) {
        if (description.length() > ContactConstants.MAX_DESC_LENGTH) {
            return false;
        }
        Pattern pattern = Pattern.compile(ContactConstants.DESC_REGEX);
        Matcher matcher = pattern.matcher(description);
        return matcher.matches();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile(ContactConstants.PHONE_REGEX);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(ContactConstants.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Checks to see if contact containing same Firstname and LastName is present in the list.
     * @param inputContact
     *  Request Contact
     * @param listOfContacts
     *  Contacts already added
     * @return
     *  Return error if duplicate contact found.
     */
    public static ContactError checkForDuplicateContact(Contact inputContact, Map<Integer, Contact> listOfContacts) {
        ContactError contactError = null;
        Optional<Contact> foundDuplicate = listOfContacts.values().stream()
                .filter(contact -> Objects.equals(contact.getSurname(), inputContact.getSurname()))
                .filter(contact -> Objects.equals(contact.getFirstname(), inputContact.getFirstname()))
                .findAny();
        if (foundDuplicate.isPresent()) {
            return new ContactError(
                    ContactErrorCodes.DUPLICATE_ENTRY,
                    ContactErrorConstants.DUPLICATE_CONTACT);
        }
        return contactError;
    }

    public static Contact mapResponseBody(Contact contact) {
        Contact responseContact = new Contact();
        responseContact.setId(contact.getId());
        responseContact.setFirstname(contact.getFirstname());
        responseContact.setSurname(contact.getSurname());
        if (contact.getContactDescription() != null) {
            responseContact.setContactDescription(contact.getContactDescription());
        }
        if (contact.getPhoneNumber() != null) {
            responseContact.setPhoneNumber(contact.getPhoneNumber());
        }
        if (contact.getEmail() != null) {
            responseContact.setEmail(contact.getEmail());
        }
        return responseContact;
    }
}
